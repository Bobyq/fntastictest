# Тестовое Задание для Fnastic

## В Папке First Находится C++ Задание
## В Папке Second Находится Unreal Enigne Задание

### Докуметация к Проекту
1. Все Файлы Blueprints находятся в ThirdPersonBP/Blueprints
2. Модели ThirdPersonBP/Models
3. Звуки ThirdPersonBP/Audio

1.1 В папке Turtles находится вся основная логика игры

1.2.
 В Папке Turtles/Behaviours Находятся BehaviourTree Для Черепашек(В моем случае львов)

1.2.1 В Папках Turtle One, Two,Three находятся BehaviourTree, Blackboard и Task

# 1.2 SpawnButton
Для Создания Кнопки необходимо выставить её на сцену и указать Номер Льва в Details (Call turtle Number)

# 1.3 Создание новой черепахи
Для создания новой черепахи необходимо
1. Создать BehaviourTree и BlackBoard Черепахи в пути Turtles/Behaviours
2. Переместить на сцену TurtleSpawn и в Tag указать номер черепахи
3. В TurtleController найти массив BTAsset и добавить елемент, номер которого соответствует номеру черепахи(Нумерация идет с единицы)
4. Добавить на сцену TargetPoint и указать в Tag Turtle$NUM$Point
5. Создать объект наследуемый от BTTask Base и унаследовать вручную логику из прошлого такого же объекта и в GetAllActorOfClass указать Tag из TargetPoint

Черепаха создана(Лев)



   

# Сжатая документация по основной необходимой логики для работы

### Спасибо!
Author: Egor Rebrov