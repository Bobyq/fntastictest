﻿/* 
Тестовое задание для Fntastic;
Author: Egor Rebrov;

Documentation:
	ConvertToBracket() - Функция конвертирующая строку в скобки
	Input: 
		type: String - Строка для Конвертации

	Output:
		type: String - Конвертированная строка

*/

// Инициализация библиотек
#include <iostream>
#include <string>
#include <algorithm>

// Подключение пространатва имен
using namespace std;

// Инициализация типов
typedef unsigned int uint;


// Функция конвертация строки
string ConvertToBracket(string Str) {
	transform(Str.begin(), Str.end(), Str.begin(), ::toupper);
	for (uint i = 0; i < Str.length(); i++) {
		bool Changed = false;
		
		for (uint b = 0; b < Str.length(); b++) {

			if (Str[i] == Str[b] && i != b) {
				Str[b] = ')';
				Changed = true;
			} // endif;

		} // end for b;

		if (Str[i] != ')') Str[i] = '(';
		if (Changed) Str[i] = ')';
		

	} // end for i;
	return Str;
}
 
// Главная функция
int main() {
	string ConvertText;

	getline(cin, ConvertText);

	cout << ConvertToBracket(ConvertText) << endl;

}




